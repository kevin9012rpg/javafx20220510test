package rocks.imsofa.javafxpractice20220510;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;


/**
 * JavaFX App
 */
public class App extends Application {

    @Override
    public void start(Stage stage) {
        BorderPane borderPane=new BorderPane();
        FlowPane flowPane=new FlowPane();
        Pane container=new Pane();
        borderPane.setCenter(container);
        borderPane.setTop(flowPane);
        Scene scene=new Scene(borderPane, 800, 600);
        stage.setScene(scene);
        Circle circle=new Circle();
        circle.setCenterX(400);
        circle.setCenterY(300);
        circle.setRadius(50);
        
        container.getChildren().add(circle);
        
        
        Button upBt=new Button("UP");
        Button rightBt=new Button("RIGHT");
        Button downBt=new Button("DOWN");
        Button leftBt=new Button("LEFT");
        flowPane.getChildren().addAll(upBt, rightBt, downBt, leftBt);
        
        
        
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }

}